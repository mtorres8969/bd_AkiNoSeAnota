class Guest < ActiveRecord::Base
  belongs_to :person
  belongs_to :meeting
  validates :person, :meeting, presence: true
end
