class Topic < ActiveRecord::Base
	belongs_to :meeting
	has_many :tasks
	validates :title, :agreement, :time, presence: true
end
