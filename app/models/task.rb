class Task < ActiveRecord::Base
  belongs_to :guest
  belongs_to :topic
  validates :labor, :date_end, presence: true
  validates :guest, :topic, presence: true
end
