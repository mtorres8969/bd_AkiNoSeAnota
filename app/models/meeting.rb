class Meeting < ActiveRecord::Base
  validates :subject, :place, :date, :time, presence: true
	has_many :people, through: :guests
	has_many :topics

  before_create do
    self.subject = subject.capitalize
  end
end
