class Person < ActiveRecord::Base
  has_many :meetings, through: :guests
  validates :name, :last_name, :email,  presence: true
  validates :phone, uniqueness: true
  
  before_validation :troll

  def troll
    if email.nil?
      self.email = "Sin email? Seria una lastima que te añadieran en un buen trabajo y no tengamos luego como comunicarnos contigo :3"
    end
  end
end
