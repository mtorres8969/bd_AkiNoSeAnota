class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :title
      t.string :time
      t.string :agreement

      t.timestamps null: false
    end
  end
end
