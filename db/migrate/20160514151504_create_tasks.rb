class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :labor
      t.date :date_end
      t.boolean :status
    
      t.timestamps null: false
    end
  end
end
