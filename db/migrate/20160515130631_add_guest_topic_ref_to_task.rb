class AddGuestTopicRefToTask < ActiveRecord::Migration
  def change
    add_reference :tasks, :topic, index: true, foreign_key: true
    add_reference :tasks, :guest, index: true, foreign_key: true
  end
end
