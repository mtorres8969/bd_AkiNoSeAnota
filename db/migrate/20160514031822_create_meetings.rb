class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.date :date
      t.string :time
      t.string :place
      t.string :subject

      t.timestamps null: false
    end
  end
end
