class AddMeetingRefToTopic < ActiveRecord::Migration
  def change
    add_reference :topics, :meeting, index: true, foreign_key: true
  end
end
