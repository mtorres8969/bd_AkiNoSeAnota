# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160515130631) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "guests", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "meeting_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "guests", ["meeting_id"], name: "index_guests_on_meeting_id", using: :btree
  add_index "guests", ["person_id"], name: "index_guests_on_person_id", using: :btree

  create_table "meetings", force: :cascade do |t|
    t.date     "date"
    t.string   "time"
    t.string   "place"
    t.string   "subject"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", force: :cascade do |t|
    t.string   "name"
    t.string   "last_name"
    t.string   "email"
    t.integer  "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "labor"
    t.date     "date_end"
    t.boolean  "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "topic_id"
    t.integer  "guest_id"
  end

  add_index "tasks", ["guest_id"], name: "index_tasks_on_guest_id", using: :btree
  add_index "tasks", ["topic_id"], name: "index_tasks_on_topic_id", using: :btree

  create_table "topics", force: :cascade do |t|
    t.string   "title"
    t.string   "time"
    t.string   "agreement"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "meeting_id"
  end

  add_index "topics", ["meeting_id"], name: "index_topics_on_meeting_id", using: :btree

  add_foreign_key "guests", "meetings"
  add_foreign_key "guests", "people"
  add_foreign_key "tasks", "guests"
  add_foreign_key "tasks", "topics"
  add_foreign_key "topics", "meetings"
end
