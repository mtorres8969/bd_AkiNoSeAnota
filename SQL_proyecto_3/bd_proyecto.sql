﻿﻿CREATE TABLE personas (id serial PRIMARY KEY NOT NULL, nombre varchar(25), apellido varchar(25), correo varchar(25), 
telefono integer);

CREATE TABLE reuniones (id serial PRIMARY KEY NOT NULL, hora TIME, fecha DATE, lugar varchar(25),
asunto varchar(50));

CREATE TABLE persona_reunion (id serial PRIMARY KEY NOT NULL, persona_id integer 
REFERENCES personas (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, reunion_id integer 
REFERENCES reuniones (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE temas (id serial PRIMARY KEY NOT NULL, titulo varchar(25), estimado TIME, acuerdo varchar(50), reunion_id integer 
REFERENCES reuniones (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE);

CREATE TABLE tareas (id serial PRIMARY KEY NOT NULL, deber varchar(50), fecha_a_culminar DATE, status BOOLEAN, responsable_id integer
REFERENCES persona_reunion (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, temas_id integer 
REFERENCES temas (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE)